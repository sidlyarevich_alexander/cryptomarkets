package com.starsirius.kotlinsampleappcryptomarkets.data.repository

import com.starsirius.kotlinsampleappcryptomarkets.data.domain.ExchangesApi
import com.starsirius.kotlinsampleappcryptomarkets.data.model.ExchangesItem
import com.starsirius.kotlinsampleappcryptomarkets.data.model.core.Result
import com.starsirius.kotlinsampleappcryptomarkets.data.retrofit.CoinGeckoApi

class ExchangeRepository(private val coinGeckoApi: CoinGeckoApi) : BaseRepository(),
    ExchangesApi {

    override suspend fun exchangesList(perPage: Int, page: String): Result<List<ExchangesItem>> =
        safeApiCall({ coinGeckoApi.exchanges(perPage, page) })
}