package com.starsirius.kotlinsampleappcryptomarkets.data.domain

import com.starsirius.kotlinsampleappcryptomarkets.data.model.ExchangesItem
import com.starsirius.kotlinsampleappcryptomarkets.data.model.core.Result

interface ExchangesApi {
    suspend fun exchangesList(perPage: Int, page: String): Result<List<ExchangesItem>>
}