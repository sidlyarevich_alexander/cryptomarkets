package com.starsirius.kotlinsampleappcryptomarkets.data.repository

import com.starsirius.kotlinsampleappcryptomarkets.data.model.core.Result
import retrofit2.Response
import java.io.IOException

abstract class BaseRepository {
    suspend fun <T : Any> safeApiCall(
        call: suspend () -> Response<T>,
        errorMessage: String = ""
    ): Result<T> {
        return safeApiResult(call, errorMessage)
    }

    private suspend fun <T : Any> safeApiResult(
        call: suspend () -> Response<T>,
        errorMessage: String
    ): Result<T> {
        return try {
            val response = call.invoke()
            if (response.isSuccessful) {
                Result.Success(response.body()!!)
            } else {
                Result.Error(IOException("Error - ${response.errorBody()?.string()}"))
            }
        } catch (e: IOException) {
            Result.Error(IOException("Error IOException - ${e.message}"))
        }
    }
}