package com.starsirius.kotlinsampleappcryptomarkets.data.retrofit

import com.starsirius.kotlinsampleappcryptomarkets.data.model.ExchangesItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CoinGeckoApi {

    @GET("exchanges")
    suspend fun exchanges(
        @Query("per_page") perPage: Int,
        @Query("page") page: String
    ): Response<List<ExchangesItem>>

    companion object {
        const val COIN_GECKO_API_URL = "https://api.coingecko.com/api/v3/"
    }
}