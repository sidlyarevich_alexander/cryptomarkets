package com.starsirius.kotlinsampleappcryptomarkets.di

import com.google.gson.Gson
import com.starsirius.kotlinsampleappcryptomarkets.data.retrofit.CoinGeckoApi
import com.starsirius.kotlinsampleappcryptomarkets.data.retrofit.CoinGeckoApi.Companion.COIN_GECKO_API_URL
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val retrofitModule = module {
    single { Gson() }
    single {
        OkHttpClient
            .Builder()
            .build()
    }
    single<CoinGeckoApi> {
        Retrofit
            .Builder()
            .baseUrl(COIN_GECKO_API_URL)
            .client(get())
            .addConverterFactory(GsonConverterFactory.create(get()))
            .build()
            .create(CoinGeckoApi::class.java)
    }
}