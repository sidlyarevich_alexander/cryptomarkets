package com.starsirius.kotlinsampleappcryptomarkets.di

import com.starsirius.kotlinsampleappcryptomarkets.ui.adapters.ExchangeAdapter
import org.koin.dsl.module

val exchangeAdapterModule = module {
    single { (callback: (String) -> Unit) -> ExchangeAdapter(callback) }
}