package com.starsirius.kotlinsampleappcryptomarkets.di

import com.starsirius.kotlinsampleappcryptomarkets.data.domain.ExchangesApi
import com.starsirius.kotlinsampleappcryptomarkets.data.repository.ExchangeRepository
import org.koin.dsl.module

val exchangeRepositoryModule = module {
    single<ExchangesApi> { ExchangeRepository(get()) }
}