package com.starsirius.kotlinsampleappcryptomarkets.di

import com.starsirius.kotlinsampleappcryptomarkets.ui.ExchangeViewModel
import org.koin.dsl.module

val exchangeViewModelModule = module {
    single { ExchangeViewModel(get()) }
}