package com.starsirius.kotlinsampleappcryptomarkets.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.starsirius.kotlinsampleappcryptomarkets.R
import com.starsirius.kotlinsampleappcryptomarkets.data.model.ExchangesItem
import kotlinx.android.synthetic.main.exchange_item.view.*

class ExchangeAdapter(private val onClick: (String) -> Unit) :
    RecyclerView.Adapter<ExchangeAdapter.ExchangeViewHolder>() {

    var exchangeItemList: List<ExchangesItem> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExchangeViewHolder =
        ExchangeViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.exchange_item, parent, false)
        )

    override fun onBindViewHolder(holder: ExchangeViewHolder, position: Int) {
        holder.bind(onClick, exchangeItemList[position].url)
        holder.setExchangeTitle(exchangeItemList[position].name)
        holder.setExchangeIcon(exchangeItemList[position].image)
    }

    override fun getItemCount(): Int {
        return exchangeItemList.size
    }

    class ExchangeViewHolder(private val baseView: View) : RecyclerView.ViewHolder(baseView) {

        fun bind(onClick: (String) -> Unit, url: String) {
            baseView.setOnClickListener {
                onClick(url)
            }
        }

        fun setExchangeTitle(text: String) {
            baseView.text_exchange.text = text
        }

        fun setExchangeIcon(url: String) {
            baseView.image_icon.load(url) {
                placeholder(R.drawable.ic_exchange_placeholder)
                error(R.drawable.ic_exchange_placeholder)
            }
        }
    }
}