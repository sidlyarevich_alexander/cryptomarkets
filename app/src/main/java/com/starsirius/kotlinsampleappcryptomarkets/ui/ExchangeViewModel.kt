package com.starsirius.kotlinsampleappcryptomarkets.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.starsirius.kotlinsampleappcryptomarkets.data.domain.ExchangesApi
import com.starsirius.kotlinsampleappcryptomarkets.data.model.ExchangesItem
import com.starsirius.kotlinsampleappcryptomarkets.data.model.core.Result
import kotlinx.coroutines.launch

class ExchangeViewModel(private val repository: ExchangesApi) : ViewModel() {
    val exchangesList = MutableLiveData<List<ExchangesItem>>()
    val errorString = MutableLiveData<String>()

    fun fetchData(perPage: Int = 100, page: String = "1") {
        viewModelScope.launch {
            when (val exchanges = repository.exchangesList(perPage, page)) {
                is Result.Success -> {
                    exchangesList.value = exchanges.data
                }
                is Result.Error -> {
                    errorString.value = exchanges.exception.message
                }
            }
        }
    }
}