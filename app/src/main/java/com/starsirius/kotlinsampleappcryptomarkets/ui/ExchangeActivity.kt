package com.starsirius.kotlinsampleappcryptomarkets.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.starsirius.kotlinsampleappcryptomarkets.R
import com.starsirius.kotlinsampleappcryptomarkets.ui.adapters.ExchangeAdapter
import kotlinx.android.synthetic.main.activity_exchange.*
import kotlinx.android.synthetic.main.exchange_item.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ExchangeActivity : AppCompatActivity() {

    private val viewModel by viewModel<ExchangeViewModel>()

    private val adapter by inject<ExchangeAdapter> { parametersOf(::onClick) }

    private val loadURL: ActivityResultLauncher<Intent> =
        registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exchange)

        viewModel.fetchData()

        viewModel.exchangesList.observe(this, {
            adapter.exchangeItemList = it
            adapter.notifyDataSetChanged()
        })

        viewModel.errorString.observe(this, {
            text_exchange.text = it
        })
        exchange_recyclerview.adapter = adapter
    }

    private fun onClick(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        loadURL.launch(intent)
    }
}