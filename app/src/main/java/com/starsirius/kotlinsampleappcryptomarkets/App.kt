package com.starsirius.kotlinsampleappcryptomarkets

import android.app.Application
import com.starsirius.kotlinsampleappcryptomarkets.di.exchangeAdapterModule
import com.starsirius.kotlinsampleappcryptomarkets.di.exchangeRepositoryModule
import com.starsirius.kotlinsampleappcryptomarkets.di.exchangeViewModelModule
import com.starsirius.kotlinsampleappcryptomarkets.di.retrofitModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                retrofitModule, exchangeRepositoryModule, exchangeViewModelModule,
                exchangeAdapterModule
            )
        }
    }
}